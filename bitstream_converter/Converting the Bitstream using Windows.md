#Steps#

1. Generate bitstream through Vivado naturally. Ignore any warnings.

2. Copy bitstream to bitstream_converter folder. Ensure it's name is *elink2_top_wrapper.bit*

3. If running Windows, use the 'Xilinx Software Command Line Tool'. This will allow you to execute the following code easily.

```
cd to->bitstream_converter
bootgen -image bit2bin.bif -split bin

```

4. Rename *elink2_top_wrapper.bit.bin* to *parallella.bit.bin*. Copy onto SD card, removing previous bitstream, and voila it works :D