proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
set_msg_config  -ruleid {2147483647}  -severity {INFO}  -suppress 

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_property design_mode GateLvl [current_fileset]
  set_property webtalk.parent_dir C:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.cache/wt [current_project]
  set_property parent.project_path C:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.xpr [current_project]
  set_property ip_repo_paths {
  c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.cache/ip
  C:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.ipdefs/src
  C:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.ipdefs/ip_repo
  C:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.ipdefs/elink-gold
} [current_project]
  set_property ip_output_repo c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.cache/ip [current_project]
  add_files -quiet C:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.runs/synth_1/elink2_top_wrapper.dcp
  read_xdc -ref elink2_top_fifo_103x32_write_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_write_0/elink2_top_fifo_103x32_write_0/elink2_top_fifo_103x32_write_0.xdc
  set_property processing_order EARLY [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_write_0/elink2_top_fifo_103x32_write_0/elink2_top_fifo_103x32_write_0.xdc]
  read_xdc -ref elink2_top_fifo_103x16_rresp_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_rresp_0/elink2_top_fifo_103x16_rresp_0/elink2_top_fifo_103x16_rresp_0.xdc
  set_property processing_order EARLY [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_rresp_0/elink2_top_fifo_103x16_rresp_0/elink2_top_fifo_103x16_rresp_0.xdc]
  read_xdc -ref elink2_top_fifo_103x16_write_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_write_0/elink2_top_fifo_103x16_write_0/elink2_top_fifo_103x16_write_0.xdc
  set_property processing_order EARLY [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_write_0/elink2_top_fifo_103x16_write_0/elink2_top_fifo_103x16_write_0.xdc]
  read_xdc -ref elink2_top_fifo_103x16_rdreq_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_rdreq_0/elink2_top_fifo_103x16_rdreq_0/elink2_top_fifo_103x16_rdreq_0.xdc
  set_property processing_order EARLY [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_rdreq_0/elink2_top_fifo_103x16_rdreq_0/elink2_top_fifo_103x16_rdreq_0.xdc]
  read_xdc -ref elink2_top_fifo_103x32_rresp_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_rresp_0/elink2_top_fifo_103x32_rresp_0/elink2_top_fifo_103x32_rresp_0.xdc
  set_property processing_order EARLY [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_rresp_0/elink2_top_fifo_103x32_rresp_0/elink2_top_fifo_103x32_rresp_0.xdc]
  read_xdc -ref elink2_top_fifo_103x32_rdreq_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_rdreq_0/elink2_top_fifo_103x32_rdreq_0/elink2_top_fifo_103x32_rdreq_0.xdc
  set_property processing_order EARLY [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_rdreq_0/elink2_top_fifo_103x32_rdreq_0/elink2_top_fifo_103x32_rdreq_0.xdc]
  read_xdc -ref elink2_top_processing_system7_0_0 -cells inst c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_processing_system7_0_0/elink2_top_processing_system7_0_0.xdc
  set_property processing_order EARLY [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_processing_system7_0_0/elink2_top_processing_system7_0_0.xdc]
  read_xdc -prop_thru_buffers -ref elink2_top_proc_sys_reset_0_0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_proc_sys_reset_0_0/elink2_top_proc_sys_reset_0_0_board.xdc
  set_property processing_order EARLY [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_proc_sys_reset_0_0/elink2_top_proc_sys_reset_0_0_board.xdc]
  read_xdc -ref elink2_top_proc_sys_reset_0_0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_proc_sys_reset_0_0/elink2_top_proc_sys_reset_0_0.xdc
  set_property processing_order EARLY [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_proc_sys_reset_0_0/elink2_top_proc_sys_reset_0_0.xdc]
  read_xdc C:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/constrs_1/imports/constraints/parallella_timing.xdc
  read_xdc C:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/constrs_1/imports/constraints/parallella_z70x0_loc.xdc
  read_xdc C:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/constrs_1/imports/constraints/parallella_z7020_loc.xdc
  read_xdc -ref elink2_top_fifo_103x32_write_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_write_0/elink2_top_fifo_103x32_write_0/elink2_top_fifo_103x32_write_0_clocks.xdc
  set_property processing_order LATE [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_write_0/elink2_top_fifo_103x32_write_0/elink2_top_fifo_103x32_write_0_clocks.xdc]
  read_xdc -ref elink2_top_fifo_103x16_rresp_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_rresp_0/elink2_top_fifo_103x16_rresp_0/elink2_top_fifo_103x16_rresp_0_clocks.xdc
  set_property processing_order LATE [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_rresp_0/elink2_top_fifo_103x16_rresp_0/elink2_top_fifo_103x16_rresp_0_clocks.xdc]
  read_xdc -ref elink2_top_fifo_103x16_write_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_write_0/elink2_top_fifo_103x16_write_0/elink2_top_fifo_103x16_write_0_clocks.xdc
  set_property processing_order LATE [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_write_0/elink2_top_fifo_103x16_write_0/elink2_top_fifo_103x16_write_0_clocks.xdc]
  read_xdc -ref elink2_top_fifo_103x16_rdreq_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_rdreq_0/elink2_top_fifo_103x16_rdreq_0/elink2_top_fifo_103x16_rdreq_0_clocks.xdc
  set_property processing_order LATE [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x16_rdreq_0/elink2_top_fifo_103x16_rdreq_0/elink2_top_fifo_103x16_rdreq_0_clocks.xdc]
  read_xdc -ref elink2_top_fifo_103x32_rresp_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_rresp_0/elink2_top_fifo_103x32_rresp_0/elink2_top_fifo_103x32_rresp_0_clocks.xdc
  set_property processing_order LATE [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_rresp_0/elink2_top_fifo_103x32_rresp_0/elink2_top_fifo_103x32_rresp_0_clocks.xdc]
  read_xdc -ref elink2_top_fifo_103x32_rdreq_0 -cells U0 c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_rdreq_0/elink2_top_fifo_103x32_rdreq_0/elink2_top_fifo_103x32_rdreq_0_clocks.xdc
  set_property processing_order LATE [get_files c:/redback-hardware/parallella_7020_headless_gpiose_elink2/parallella_7020_headless_gpiose_elink2.srcs/sources_1/bd/elink2_top/ip/elink2_top_fifo_103x32_rdreq_0/elink2_top_fifo_103x32_rdreq_0/elink2_top_fifo_103x32_rdreq_0_clocks.xdc]
  link_design -top elink2_top_wrapper -part xc7z020clg400-2
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  catch {write_debug_probes -quiet -force debug_nets}
  opt_design 
  write_checkpoint -force elink2_top_wrapper_opt.dcp
  report_drc -file elink2_top_wrapper_drc_opted.rpt
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  catch {write_hwdef -file elink2_top_wrapper.hwdef}
  place_design 
  write_checkpoint -force elink2_top_wrapper_placed.dcp
  report_io -file elink2_top_wrapper_io_placed.rpt
  report_utilization -file elink2_top_wrapper_utilization_placed.rpt -pb elink2_top_wrapper_utilization_placed.pb
  report_control_sets -verbose -file elink2_top_wrapper_control_sets_placed.rpt
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force elink2_top_wrapper_routed.dcp
  report_drc -file elink2_top_wrapper_drc_routed.rpt -pb elink2_top_wrapper_drc_routed.pb
  report_timing_summary -warn_on_violation -max_paths 10 -file elink2_top_wrapper_timing_summary_routed.rpt -rpx elink2_top_wrapper_timing_summary_routed.rpx
  report_power -file elink2_top_wrapper_power_routed.rpt -pb elink2_top_wrapper_power_summary_routed.pb
  report_route_status -file elink2_top_wrapper_route_status.rpt -pb elink2_top_wrapper_route_status.pb
  report_clock_utilization -file elink2_top_wrapper_clock_utilization_routed.rpt
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

