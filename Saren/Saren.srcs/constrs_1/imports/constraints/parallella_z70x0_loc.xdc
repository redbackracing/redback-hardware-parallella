###############################################################
##  Location constraints for the Parallella-I board
##  3/12/14 F. Huettig
##  Updated to XDC format 7/1/14 F. Huettig
####
## This file defines pin locations & standards for the Parallella-I
##   and Zynq 7010 or 7020.  See the file parallella_z7020_loc.ucf
##    for pins added with the 7020.
## Timing constraints are defined elsewhere.
###############################################################

#  NOTE:  IOSTANDARDS for e-link and gpio have been removed
#    from these files.  IOSTANDARDS are to be set in the
#    verilog instead.

#######################
# Configuration Pins
#######################
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]

#####################
# I2C
#####################
set_property PACKAGE_PIN N18 [get_ports I2C_SCL]
set_property IOSTANDARD LVCMOS33 [get_ports I2C_SCL]
set_property PACKAGE_PIN N17 [get_ports I2C_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports I2C_SDA]

#####################
# Epiphany Interface
#####################
set_property PACKAGE_PIN G14 [get_ports {DSP_RESET_N[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DSP_RESET_N[0]}]
set_property DRIVE 4 [get_ports {DSP_RESET_N[0]}]

set_property PACKAGE_PIN H17 [get_ports CCLK_N]
set_property PACKAGE_PIN F17 [get_ports TX_lclk_n]
set_property PACKAGE_PIN A20 [get_ports {TX_data_n[0]}]
set_property PACKAGE_PIN B20 [get_ports {TX_data_n[1]}]
set_property PACKAGE_PIN D20 [get_ports {TX_data_n[2]}]
set_property PACKAGE_PIN E19 [get_ports {TX_data_n[3]}]
set_property PACKAGE_PIN D18 [get_ports {TX_data_n[4]}]
set_property PACKAGE_PIN F20 [get_ports {TX_data_n[5]}]
set_property PACKAGE_PIN G18 [get_ports {TX_data_n[6]}]
set_property PACKAGE_PIN G20 [get_ports {TX_data_n[7]}]
set_property PACKAGE_PIN G15 [get_ports TX_frame_n]
set_property PACKAGE_PIN J15 [get_ports TX_rd_wait_p]
set_property IOSTANDARD LVCMOS25 [get_ports TX_rd_wait_p]
#NET "RXO_RD_WAIT_N" LOC = "H17";
set_property PACKAGE_PIN H18 [get_ports TX_wr_wait_n]
set_property PACKAGE_PIN K18 [get_ports RX_lclk_n]
set_property PACKAGE_PIN J19 [get_ports {RX_data_n[0]}]
set_property PACKAGE_PIN L15 [get_ports {RX_data_n[1]}]
set_property PACKAGE_PIN L17 [get_ports {RX_data_n[2]}]
set_property PACKAGE_PIN M15 [get_ports {RX_data_n[3]}]
set_property PACKAGE_PIN L20 [get_ports {RX_data_n[4]}]
set_property PACKAGE_PIN M20 [get_ports {RX_data_n[5]}]
set_property PACKAGE_PIN M18 [get_ports {RX_data_n[6]}]
set_property PACKAGE_PIN N16 [get_ports {RX_data_n[7]}]
set_property PACKAGE_PIN H20 [get_ports RX_frame_n]
set_property PACKAGE_PIN J14 [get_ports RX_rd_wait_n]
set_property PACKAGE_PIN J16 [get_ports RX_wr_wait_n]

#######################
# GPIO Location
#######################
set_property PACKAGE_PIN T16 [get_ports {GPIO_P[0]}]
set_property PACKAGE_PIN U17 [get_ports {GPIO_N[0]}]
set_property PACKAGE_PIN V16 [get_ports {GPIO_P[1]}]
set_property PACKAGE_PIN W16 [get_ports {GPIO_N[1]}]
set_property PACKAGE_PIN P15 [get_ports {GPIO_P[2]}]
set_property PACKAGE_PIN P16 [get_ports {GPIO_N[2]}]
set_property PACKAGE_PIN U18 [get_ports {GPIO_P[3]}]
set_property PACKAGE_PIN U19 [get_ports {GPIO_N[3]}]
set_property PACKAGE_PIN P14 [get_ports {GPIO_P[4]}]
set_property PACKAGE_PIN R14 [get_ports {GPIO_N[4]}]
set_property PACKAGE_PIN T14 [get_ports {GPIO_P[5]}]
set_property PACKAGE_PIN T15 [get_ports {GPIO_N[5]}]
set_property PACKAGE_PIN U14 [get_ports {GPIO_P[6]}]
set_property PACKAGE_PIN U15 [get_ports {GPIO_N[6]}]
set_property PACKAGE_PIN W14 [get_ports {GPIO_P[7]}]
set_property PACKAGE_PIN Y14 [get_ports {GPIO_N[7]}]
set_property PACKAGE_PIN U13 [get_ports {GPIO_P[8]}]
set_property PACKAGE_PIN V13 [get_ports {GPIO_N[8]}]
set_property PACKAGE_PIN V12 [get_ports {GPIO_P[9]}]
set_property PACKAGE_PIN W13 [get_ports {GPIO_N[9]}]
set_property PACKAGE_PIN T12 [get_ports {GPIO_P[10]}]
set_property PACKAGE_PIN U12 [get_ports {GPIO_N[10]}]
set_property PACKAGE_PIN T11 [get_ports {GPIO_P[11]}]
set_property PACKAGE_PIN T10 [get_ports {GPIO_N[11]}]
set_property PACKAGE_PIN Y12 [get_ports {GPIO_P[12]}]
set_property PACKAGE_PIN Y13 [get_ports {GPIO_N[12]}]
set_property PACKAGE_PIN W11 [get_ports {GPIO_P[13]}]
set_property PACKAGE_PIN Y11 [get_ports {GPIO_N[13]}]
set_property PACKAGE_PIN V11 [get_ports {GPIO_P[14]}]
set_property PACKAGE_PIN V10 [get_ports {GPIO_N[14]}]
set_property PACKAGE_PIN T9 [get_ports {GPIO_P[15]}]
set_property PACKAGE_PIN U10 [get_ports {GPIO_N[15]}]
set_property PACKAGE_PIN W10 [get_ports {GPIO_P[16]}]
set_property PACKAGE_PIN W9 [get_ports {GPIO_N[16]}]
set_property PACKAGE_PIN U9 [get_ports {GPIO_P[17]}]
set_property PACKAGE_PIN U8 [get_ports {GPIO_N[17]}]
set_property PACKAGE_PIN V8 [get_ports {GPIO_P[18]}]
set_property PACKAGE_PIN W8 [get_ports {GPIO_N[18]}]
set_property PACKAGE_PIN Y9 [get_ports {GPIO_P[19]}]
set_property PACKAGE_PIN Y8 [get_ports {GPIO_N[19]}]
set_property PACKAGE_PIN Y7 [get_ports {GPIO_P[20]}]
set_property PACKAGE_PIN Y6 [get_ports {GPIO_N[20]}]
set_property PACKAGE_PIN U7 [get_ports {GPIO_P[21]}]
set_property PACKAGE_PIN V7 [get_ports {GPIO_N[21]}]
set_property PACKAGE_PIN V6 [get_ports {GPIO_P[22]}]
set_property PACKAGE_PIN W6 [get_ports {GPIO_N[22]}]
set_property PACKAGE_PIN T5 [get_ports {GPIO_P[23]}]
set_property PACKAGE_PIN U5 [get_ports {GPIO_N[23]}]

#######################
# GPIO Power
#######################
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[16]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[16]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[17]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[17]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[18]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[18]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[19]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[19]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[20]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[20]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[21]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[21]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[22]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[22]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_P[23]}]
set_property IOSTANDARD LVCMOS33 [get_ports {GPIO_N[23]}]
