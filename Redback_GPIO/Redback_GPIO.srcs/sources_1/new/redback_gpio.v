`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Redback Racing
// Engineer: Mrinal Chakravarthy
// 
// Create Date: 02.06.2016 07:48:02
// Design Name: Redback GPIO Module
// Module Name: redback_gpio
// Project Name: Redback Data logger
// Target Devices: Parallella
// Description: Acts as a unique GPIO interface to allow flexibility in sensor usage
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module redback_gpio(
    input CAN_TX,
    output CAN_RX,
    input [63:0] PS_GPIO_T,
    input [63:0] PS_GPIO_O,
    output[63:0] PS_GPIO_I,
    inout [23:0] GPIO_P,
    inout [23:0] GPIO_N
    );
    
    /** CAN connection */
    assign CAN_TX = GPIO_P[23];
    assign CAN_RX = GPIO_N[23];
    
    /** Tie up GPIO on PS for now **/
    genvar GPIO_PS;
    generate
    for(GPIO_PS = 0; GPIO_PS < 64; GPIO_PS=GPIO_PS+1) begin
        assign PS_GPIO_I[GPIO_PS] = PS_GPIO_O[GPIO_PS] & PS_GPIO_T[GPIO_PS];
    end
    endgenerate
   
    /** Tie up unused pairs */
    genvar TIE_UP;
    generate
    for(TIE_UP = 0; TIE_UP < 23; TIE_UP = TIE_UP+1) begin 
        assign GPIO_P[TIE_UP] = GPIO_N[TIE_UP];
    end
    endgenerate
    
endmodule
