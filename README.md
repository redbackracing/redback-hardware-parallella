# README #

### What is this repository for? ###

Repository to store the hardware files required to build upon the Parallella FPGA for Data Acqusition.

### Dependencies ###

* Vivado 2015.4 - Please install the free edition with SDK attached. Need SDK for boot image tools

* Windows/Linux (due to Vivado)

### Newcomers ###

If you're a newcomer, then attempt this [tutorial](https://www.parallella.org/2015/03/23/new-parallella-elink-fpga-design-project-now-available-in-vivado/) first using our repository. We have the parallella hardware encased within. For the final conversion steps refer [here](https://bitbucket.org/mchakravarthy/redback-hardware/wiki/Converting%20the%20Bitstream%20using%20Windows)

A more comprehensive (but out-of-date) guide to creation of bitstream and other boot files can be found [here](http://www.adapteva.com/white-papers/building-linux-for-parallella-platform/). It provides a good outline of steps however for understanding how to create the ulimage and devicetree files. But needs to be taken with a grain of salt e.g. we don't use PlanAhead projects anymore, nor are any of the Git Links in the right spot.

If new to Verilog programming, here's a short [tutorial](http://www.verilogtutorial.info/)